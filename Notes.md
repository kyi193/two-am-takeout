This file is for taking notes as you go.

### Find server test bug

-Run server test in terminal
-Can't have server running while doing test
-"{\"OK\":true}\n" does not contain "\"ok\":true" is the error

### Find assumption errors in TestWebsockets()

-What is a []byte?
-How does it differ from a string?
-After looking at the socket file, it seems
like messages are stored as bytes. We should
be asserting the messages as bytes, not strings.

### Front end:

-Add field where user can enter their name
(find out where to initialize name)
-Should add some vertical margins for message boxes
-justify message box to center
-Display user's name
-Would ideally like to have the name appear below the chat box
(would do if I had more time but need to read up on Vuetify's docs)

### Socket server security issue:

-Couldn't seem to figure out the security vulnerability here.
I need to read up on gorilla's websocket documentation!
-Maybe something to do with ping-pong timings?

### Final touches for quality of life:

-Display user's name as the recipient
-Prevent user from sending a blank message
-Clear message box after sending message
-Add container around message/name field to add vertical margin spacing
-Move chat box to bottom of screen
-Change color scheme of chat boxes and remove border color for user name below
